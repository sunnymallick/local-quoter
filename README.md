# GBPC Local Quoting Application

This application demonstrates GBPC's quoter without needing an auth token from GBPC.

## Installation

Once this project is cloned from GitLab, run `npm install` to install all dependencies. This project already contains the quoter tarball needed to calculate the quotes.

## Steps

The first step is to initialize the project using the `Initialize Project` button on the main page. This step gathers the configuration and available powders for the quoter.

All the status messages, estimates and delivery quotes will display in the console.

Once the console states that `Demo has been initalized!`, a new panel will open up to allow you to add parts. You can add as many parts as you would like.
The parts menu will ask for a part name, dropdown for color options, how many of this part total, square footage per part, number of holes necessary, if each part requires zinc or a clear-coat, an external id and an image URL.

Once you have added all the information pertaining to a part, you can add the part to the parts list. Once you are done adding parts, you can click `Finish Adding Parts` to close the panel.

From there you can click `Get Estimate` to generate an estimate with all of your parts. An estimate object will generate in the console with an order and a quote. 


