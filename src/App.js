import React, { useState } from 'react';
import './App.css';
const quoter = require('gbpc-quoter')

function App() {

    const initialRequest = {
        parts: [],
        arrivalDate: new Date().toISOString().split('T')[0],
        volumes: "10,100"
    }

    const targetDays = 5
    const apiToken = 'YOUR-API-TOKEN'
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [partCount, setPartCount] = useState(0);
    const [perPartSqrft, setPerPartSqrft] = useState(0);
    const [holeCount, setHoleCount] = useState(1)
    const [requireZinc, setRequireZinc] = useState(false);
    const [requireClearCoat, setRequireClearCoat] = useState(false);
    const [externalId, setExternalId] = useState('');
    const [imageUrl, setImageUrl] = useState('');
    const [showForm, setShowForm] = useState(false);
    const [request, setRequest] = useState(initialRequest);
    const [parts, setParts] = useState(initialRequest.parts);
    const [availablePowders, setAvailablePowders] = useState([])
    const [estimate, setEstimate] = useState();
    const [deliveryQuote, setDeliveryQuote] = useState();
    const [finalQuote, setFinalQuote] = useState();
    const [projectReceipt, setProjectReceipt] = useState()


    async function initApp() {
        await quoter.init(apiToken)
        console.log(`Demo has been initialized!`)
        setAvailablePowders([...await quoter.getAvailablePowders()])
        setShowForm(true)
    }

    async function buildEstimate() {
        setRequest({
            parts: parts,
            arrivalDate: new Date().toISOString().split('T')[0],
            volumes: "10,100"
        })
        console.log('Parts have been added to the estimate!')
    }

    async function getEstimate() {
        if (!estimate) {
            const quote = await quoter.quote(request)
            setEstimate(quote)
            console.log('Estimate was generated!', quote)
        } else {
            console.log('You have already generated an estimate!', estimate)
        }
    }

    async function getDeliveryQuote() {
        if (!deliveryQuote) {
            const delQuote = await quoter.quoteDelivery(estimate.quote, targetDays)
            setDeliveryQuote(delQuote);
            console.log('Delivery quote was generated!', delQuote);
        }
        else {
            console.log(deliveryQuote);
        }
    }

    async function getFinalQuote() {
        if (!finalQuote) {
            const finalizedQuote = await quoter.getFinalizedQuote(request, targetDays);
            setFinalQuote(finalizedQuote);
            console.log('Here is the finalized quote!',finalizedQuote);
        }
        else {
            console.log("finalQuote: ", finalQuote);
        }
    }

    async function handleSubmit(e) {
        e.preventDefault()
        let newPartsList = parts.concat({
            id: externalId,
            name,
            color,
            partCount: parseInt(partCount),
            perPartSqrft: parseInt(perPartSqrft),
            holeCount: parseInt(holeCount),
            requireZinc,
            requireClearCoat,
            imageUrl
        })
        setParts(newPartsList)

        setName('')
        setColor('')
        setPartCount(0)
        setPerPartSqrft(0)
        setHoleCount(1)
        setRequireZinc(false)
        setRequireClearCoat(false)
        setExternalId('')
        setImageUrl('')
    }

    async function submitProject() {
        if (!projectReceipt && finalQuote) {
            const project = await quoter.submitOrder(finalQuote.quoteId, targetDays);
            setProjectReceipt(project);
            console.log('Project submitted!');
        } else {
            console.log('There is no project to submit!');
        }
    }

    function renderForm() {
        return (
            <div className="quoteFormContainer">
                <div className="quoteForm">
                    <label>Enter part details for estimate.</label>
                    <form onSubmit={handleSubmit}>
                        <input
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            placeholder="Part Name"
                        ></input>
                        <select value={color} onChange={(e) => setColor(e.target.value)}>
                            <option key={Math.random()}>Choose Color:</option>
                            {availablePowders.map((powder) => {
                                return (
                                    <option key={Math.random()}>{powder.name}</option>
                                )
                            })}
                        </select>
                        <label>Number of Parts</label>
                        <input
                            type="number"
                            min='0'
                            value={partCount}
                            onChange={(e) => setPartCount(e.target.value)}
                            placeholder="Part Count">
                        </input>
                        <label>Total Square Feet Per Part</label>
                        <input
                            type="number"
                            min='0'
                            value={perPartSqrft}
                            onChange={(e) => setPerPartSqrft(e.target.value)}
                            placeholder="Per Part Square Ft">
                        </input>
                        <label>Number of Holes in Part</label>
                        <input
                            type="number"
                            min='1'
                            value={holeCount}
                            onChange={(e) => setHoleCount(e.target.value)}
                            placeholder="Hole Count">
                        </input>
                        <div className="checkBoxContainer">
                            <label>Requires Zinc?</label>
                            <input
                                type="checkbox"
                                checked={requireZinc}
                                onChange={(e) => setRequireZinc(e.target.checked)}
                                placeholder="Require Zinc?">
                            </input>
                        </div>
                        <div className="checkBoxContainer">
                            <label>Require Clear-coat?</label>
                            <input
                                type="checkbox"
                                checked={requireClearCoat}
                                onChange={(e) => setRequireClearCoat(e.target.checked)}
                                placeholder="Require Clear-coat?">
                            </input>
                        </div>
                        <input
                            value={externalId}
                            onChange={(e) => setExternalId(e.target.value)}
                            placeholder="External Id">
                        </input>
                        <input
                            value={imageUrl}
                            onChange={(e) => setImageUrl(e.target.value)}
                            placeholder="Image URL">
                        </input>
                        <div className="submitButtonContainer">
                            <button className="submitButton">Add Part</button>
                        </div>
                    </form>
                    <p>When you are finished adding parts, click the button below.</p>
                    <button onClick={() => buildEstimate() && setShowForm(false)}>Finish Adding Parts</button>
                </div>
            </div>
        )
    }

  return (
      <div className="App">
          <div className="buttonContainer">
              <button className="appButton" onClick={() => initApp()}>Initialize Project</button>
              <button className="appButton" onClick={() => getEstimate()}>Get Estimate</button>
              <button className="appButton" onClick={() => getDeliveryQuote()}>Get Delivery Quote</button>
              <button className="appButton" onClick={() => getFinalQuote()}>Get Final Quote</button>
              <button className="appButton" onClick={() => submitProject()}>Submit Project to GBPC</button>
              <button className="appButton" onClick={() => console.log(`Config:`, quoter.getConfig())}>Get Config</button>
          </div>
          <div className="infoContainer">
              {showForm && renderForm()}
              <div className="partsContainer">
                  <h2>Parts</h2>
                  {parts.map((part) => (
                      <div className="partInfo" key={part.id}>
                          <p>Name: {part.name}</p>
                          <p>Color: {part.color}</p>
                          <p>Total Count: {part.partCount}</p>
                          <p>Sqft Per Part: {part.perPartSqrft}</p>
                          <p>Number of Holes: {part.holeCount}</p>
                      </div>
                  ))}
              </div>
          </div>
      </div>
  );
}

export default App;
