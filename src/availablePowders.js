const availablePowders = [
    {
        "id": 1,
        "name": "Anodized Bronze",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/1249/anodized-bronze-pmb-4158-dt20180830215453978-thumbnail.jpg?1535666110&siz",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 2,
        "name": "Corkey Red",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/3014/corkey-red-pps-3095-dt20190130180327485-thumbnail.jpg?1548871420&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 3,
        "name": "Peeka Blue",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/3018/peeka-blue-pps-4351-dt20190918140944659-thumbnail.jpg?1568815799&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 4,
        "name": "Escalade White",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/1826/escalade-white-pmb-5977-dt20180926214948052-thumbnail.jpg?1537998604&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 5,
        "name": "Mystic White",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/1593/mystic-white-pmb-5062-dt20181016213326784-thumbnail.jpg?1539725621&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 6,
        "name": "Army Green",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/3477/army-green-psb-4944-dt20200403170239751-thumbnail.jpg?1585933373&siz",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 7,
        "name": "Black Jack",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/5871/black-jack-uss-1522-dt20200305212216949-thumbnail.jpg?1583443352&siz",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 8,
        "name": "Super Chrome",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/5887/super-chrome-uss-4484-thumbnail.jpg?1533154456&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 9,
        "name": "Neon Green",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/4018/neon-green-pss-1221-dt20181205220121932-thumbnail.jpg?1544047304&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 10,
        "name": "Orange Glow",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/4150/orange-glow-pss-2876-dt20190313164601402-thumbnail.jpg?1552495577&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 11,
        "name": "Soft Satin White",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/4026/soft-satin-white-pss-1353-dt2018121016400314-thumbnail.jpg?1544460017&siz",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 12,
        "name": "Ink Black",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/3943/ink-black-pss-0106-dt20200306000400327-thumbnail.jpg?1583453054&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 13,
        "name": "Clear Vision",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/3010/clear-vision-pps-2974-dt20200305215835368-thumbnail.jpg?1583445541&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 14,
        "name": "Antique Bronze",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/1892/antique-bronze-ii-pmb-6407-dt20180717182900321-thumbnail.jpg?1531852155&siz",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 15,
        "name": "Stone Black",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/4014/stone-black-pss-1168-dt20200403153211847-thumbnail.jpg?1585927947&siz",
        "type": "powder coat",
        "glossLevel": "matt"
    },
    {
        "id": 16,
        "name": "Desert Nite",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/4963/desert-nite-black-pws-2859-dt20190102223751301-thumbnail.jpg?1546468685&siz",
        "type": "powder coat",
        "glossLevel": "eggshell"
    },
    {
        "id": 17,
        "name": "Zinc Primer",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/9973/zinc-primer-ess-10171-dt20190710174812981-thumbnail.jpg?1562940686&siz",
        "type": "powder coat",
        "glossLevel": "matt"
    },
    {
        "id": 18,
        "name": "Satin Black",
        "imageUrl": "https://www.columbiacoatings.com/resize/Shared/Images/Product/Satin-Black-44lbs-5-88-lb-FREE-SHIPPING/Satin-Black-44.jpg?b",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 20,
        "name": "BMW Silver",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/1932/bmw-silver-pmb-6525-dt2020041018200487-thumbnail.jpg?1586542822&siz",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 1607050543041,
        "name": "LollyPop Purple",
        "imageUrl": "https://www.prismaticpowders.com/shop/powder-coating-colors/PPS-1505/lollypop-purple",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 1607565163866,
        "name": "Speedway Gray",
        "imageUrl": "https://images.nicindustries.com/prismatic/products/1539/speedway-grey-pmb-4911-dt20190918151054984-thumbnail.jpg?1568819467&siz",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 1613775595868,
        "name": "Cerakote Blackout",
        "imageUrl": "https://images.nicindustries.com/cerakote/products/10095/blackout-e-100-dt20191211222726844-thumbnail.jpg?1578697684&siz",
        "type": "cerakote",
        "glossLevel": null
    },
    {
        "id": 1616529207384,
        "name": "Black Bronze II",
        "imageUrl": "https://www.prismaticpowders.com/shop/powder-coating-colors/PMB-2749/black-bronze-ii",
        "type": "powder coat",
        "glossLevel": "satin"
    },
    {
        "id": 1616529221691,
        "name": "Fractured Cherry",
        "imageUrl": "https://www.prismaticpowders.com/shop/powder-coating-colors/PVB-10293/fractured-cherry",
        "type": "powder coat",
        "glossLevel": "high gloss"
    },
    {
        "id": 1617127967935,
        "name": "61 Grey",
        "imageUrl": "",
        "type": "powder coat",
        "glossLevel": "matt"
    }
]

module.exports = availablePowders